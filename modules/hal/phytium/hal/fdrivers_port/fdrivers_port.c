// Phytium is pleased to support the open source community by making Zephyr-SDK available.

//
// Copyright (C) 2024 Phytium Technology Co., Ltd. All rights reserved.
//
// Licensed under the Apache-2.0 License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// https://opensource.org/license/apache-2-0
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#include "fdrivers_port.h"
#include <stdio.h>
#include <string.h>
#include <zephyr/kernel.h>

/* cache */
void FDriverDCacheRangeFlush(uintptr_t adr,size_t len)
{

}

void FDriverDCacheRangeInvalidate(uintptr_t adr,size_t len)
{

}


void FDriverICacheRangeInvalidate(void)
{

}


/* time delay */

void FDriverUdelay(u32 usec)
{
    k_busy_wait(usec) ;
}

void FDriverMdelay(u32 msec)
{
    k_busy_wait(msec*1000) ;
}

void FDriverSdelay(u32 sec)
{
    k_busy_wait(sec * 1000 *1000) ;
}

#define ELOG_LINE_BUF_SIZE               256
static char log_buf[ELOG_LINE_BUF_SIZE] = { 0 };

void FtDumpLogInfo(const char *tag,  u32 log_level, const char *log_tag_letter, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(log_buf, sizeof(log_buf), fmt, ap);
    va_end(ap);
    
    do
    {
        if (strcmp(log_tag_letter, "31") == 0)
        {
            printf("\031[31m%s:%s\031[0m \r\n", tag, log_buf);
        }
        else if (strcmp(log_tag_letter, "32") == 0)
        {
            printf("\032[32m%s:%s\032[0m \r\n", tag, log_buf);
        }
        else if (strcmp(log_tag_letter, "33") == 0)
        {
            printf("\033[33m%s:%s\033[0m \r\n", tag, log_buf);
        }
        else if (strcmp(log_tag_letter, "36") == 0)
        {
            printf("\036[36m%s:%s\036[0m \r\n", tag, log_buf);
        }
        else
        {
            printf("\033[35m%s:%s\033[0m \r\n", tag, log_buf);
        }

    }while (0);

    memset(log_buf, 0, sizeof(log_buf));
}