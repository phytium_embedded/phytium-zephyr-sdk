// Phytium is pleased to support the open source community by making Zephyr-SDK available.

//
// Copyright (C) 2024 Phytium Technology Co., Ltd. All rights reserved.
//
// Licensed under the Apache-2.0 License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// https://opensource.org/license/apache-2-0
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.


#ifndef FDRIVERS_PORT_H
#define FDRIVERS_PORT_H

#include "ftypes.h"


#include <zephyr/logging/log.h>
#include "fkernel.h"

/***************************** Include Files *********************************/
#ifdef __cplusplus
extern "C"
{
#endif

/* cache */
void FDriverDCacheRangeFlush(uintptr_t adr,size_t len);

void FDriverDCacheRangeInvalidate(uintptr_t adr,size_t len);

void FDriverICacheRangeInvalidate(void);


/* memory barrier */


#define FDRIVER_DSB() //DSB()

#define FDRIVER_DMB() //DMB()

#define FDRIVER_ISB() //ISB()


/* time delay */

void FDriverUdelay(u32 usec);

void FDriverMdelay(u32 msec);

void FDriverSdelay(u32 sec);



#define FT_LOG_NONE     0  /* No log output */
#define FT_LOG_ERROR    1  /* Critical errors, software module can not recover on its own */
#define FT_LOG_WARN     2   /* Error conditions from which recovery measures have been taken */
#define FT_LOG_INFO     3   /* Information messages which describe normal flow of events */
#define FT_LOG_DEBUG    4  /* Extra information which is not necessary for normal use (values, pointers, sizes, etc). */
#define FT_LOG_VERBOSE  5 /* Bigger chunks of debugging information, or frequent messages which can potentially flood the output. */

#define LOG_COLOR_BLACK "30"
#define LOG_COLOR_RED "31"
#define LOG_COLOR_GREEN "32"
#define LOG_COLOR_BROWN "33"
#define LOG_COLOR_BLUE "34"
#define LOG_COLOR_PURPLE "35"
#define LOG_COLOR_CYAN "36"
#define LOG_COLOR(COLOR) "\033[0;" COLOR "m"
#define LOG_BOLD(COLOR) "\033[1;" COLOR "m"
#define LOG_RESET_COLOR "\033[0m"
#define LOG_COLOR_E LOG_COLOR(LOG_COLOR_RED)
#define LOG_COLOR_W LOG_COLOR(LOG_COLOR_BROWN)
#define LOG_COLOR_I LOG_COLOR(LOG_COLOR_GREEN)
#define LOG_COLOR_D LOG_COLOR(LOG_COLOR_CYAN)
#define LOG_COLOR_V LOG_COLOR(LOG_COLOR_PURPLE)


void FtDumpLogInfo(const char *tag,  u32 log_level, const char *log_tag_letter, const char *fmt, ...) ;

#ifndef CONFIG_HAL_STANDALONE_SDK_DEBUG
#ifndef FT_DEBUG_PRINT_I
#define FT_DEBUG_PRINT_I(TAG, format, ...) 
#endif

#ifndef FT_DEBUG_PRINT_E
#define FT_DEBUG_PRINT_E(TAG, format, ...)  FtDumpLogInfo(TAG, FT_LOG_ERROR, LOG_COLOR_RED,format,##__VA_ARGS__)
#endif

#ifndef FT_DEBUG_PRINT_D
#define FT_DEBUG_PRINT_D(TAG, format, ...) 
#endif

#ifndef FT_DEBUG_PRINT_W
#define FT_DEBUG_PRINT_W(TAG, format, ...) 
#endif

#ifndef FT_DEBUG_PRINT_V
#define FT_DEBUG_PRINT_V(TAG, format, ...) 
#endif

#else
#ifndef FT_DEBUG_PRINT_I
#define FT_DEBUG_PRINT_I(TAG, format, ...) FtDumpLogInfo(TAG, FT_LOG_INFO, LOG_COLOR_PURPLE,format,##__VA_ARGS__)
#endif

#ifndef FT_DEBUG_PRINT_E
#define FT_DEBUG_PRINT_E(TAG, format, ...) FtDumpLogInfo(TAG, FT_LOG_ERROR, LOG_COLOR_RED,format,##__VA_ARGS__)
#endif

#ifndef FT_DEBUG_PRINT_D
#define FT_DEBUG_PRINT_D(TAG, format, ...)  FtDumpLogInfo(TAG, FT_LOG_DEBUG,  LOG_COLOR_GREEN,format, ##__VA_ARGS__)
#endif

#ifndef FT_DEBUG_PRINT_W
#define FT_DEBUG_PRINT_W(TAG, format, ...) FtDumpLogInfo(TAG, FT_LOG_WARN, LOG_COLOR_BROWN,format, ##__VA_ARGS__)
#endif

#ifndef FT_DEBUG_PRINT_V
#define FT_DEBUG_PRINT_V(TAG, format, ...)  FtDumpLogInfo(TAG, FT_LOG_VERBOSE, LOG_COLOR_BROWN,format, ##__VA_ARGS__)
#endif

#endif


#ifdef __cplusplus
}
#endif


#endif
