
#include <zephyr/logging/log.h>
#include "test_sched.h"
LOG_MODULE_REGISTER(app);

/* Shared threads */
K_THREAD_STACK_DEFINE(tstack, STACK_SIZE);
K_THREAD_STACK_ARRAY_DEFINE(tstacks, MAX_NUM_THREAD, STACK_SIZE);

void spin_for_ms(int ms)
{
	uint32_t t32 = k_uptime_get_32();

	while (k_uptime_get_32() - t32 < ms) {
		/* In the posix arch, a busy loop takes no time, so
		 * let's make it take some
		 */
		if (IS_ENABLED(CONFIG_ARCH_POSIX)) {
			k_busy_wait(50);
		}
	}
}



int main(void)
{
	return 0;
}


static int cmd_test_priority_scheduling(const struct shell *sh,
			      size_t argc, char **argv)
{
	test_priority_scheduling() ;
	return 0 ;
}

static int cmd_test_sched_is_preempt_thread(const struct shell *sh,
			      size_t argc, char **argv)
{
	test_sched_is_preempt_thread() ;
	return 0 ;
}

static int cmd_test_sched_priority(const struct shell *sh,
			      size_t argc, char **argv)
{
	test_priority_cooperative() ;
	test_priority_preemptible() ;
	test_priority_preemptible_wait_prio() ;
	return 0 ;
}



static int cmd_test_sched_timeslice_reset(const struct shell *sh,
			      size_t argc, char **argv)
{
	test_slice_reset() ;
	return 0 ;
}


static int cmd_test_slice_scheduling(const struct shell *sh,
			      size_t argc, char **argv)
{
	test_slice_scheduling() ;
	test_slice_perthread() ;
	return 0 ;
}

static int  cmd_test_sched_timeslice_and_lock(const struct shell *sh,
			      size_t argc, char **argv)
{
	test_yield_cooperative() ;
	test_sleep_cooperative() ;
	test_busy_wait_cooperative() ;
	test_sleep_wakeup_preemptible() ;
	test_pending_thread_wakeup() ;
	test_time_slicing_preemptible() ;
	test_time_slicing_disable_preemptible() ;
	test_lock_preemptible() ;
	test_unlock_preemptible() ;
	test_unlock_nested_sched_lock() ;
	test_wakeup_expired_timer_thread() ;
	return 0 ;
}


SHELL_STATIC_SUBCMD_SET_CREATE(sub_schedule_tests,
	SHELL_CMD(priority_scheduling, NULL, "Test priority scheduling.", cmd_test_priority_scheduling),
	SHELL_CMD(is_preempt_thread, NULL, "Test if current thread is preemptible.", cmd_test_sched_is_preempt_thread),
	SHELL_CMD(priority, NULL, "Test thread scheduling by priority.", cmd_test_sched_priority),
	SHELL_CMD(timeslice_reset, NULL, "Test timeslice resetting.", cmd_test_sched_timeslice_reset),
	SHELL_CMD(slice_scheduling, NULL, "Test scheduling with time slicing.", cmd_test_slice_scheduling),
	SHELL_CMD(timeslice_lock, NULL, "Test time slicing and locks.", cmd_test_sched_timeslice_and_lock),
	SHELL_SUBCMD_SET_END
);

/* 注册 'schedule_test' 作为 'kernel' 子命令 */
SHELL_CMD_REGISTER(schedule_test, &sub_schedule_tests, "Schedule testing commands", NULL);
