/*
 * Copyright (c) 2017 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/irq_offload.h>
#include "test_sched.h"
#include <zephyr/logging/log.h>

/* local variables */
static struct k_thread tdata;
static struct k_sem end_sema;


/**
 * @name: tpreempt_ctx
 * @msg:  This function is designed to verify the behavior of a preemptive thread with regards to scheduler locking and thread priority adjustments. It first checks if the current thread is preemptible. Then, it locks the scheduler to make the current thread non-preemptible and checks the thread's preempt status again. After unlocking the scheduler, it verifies the thread's preempt status for a third time. Lastly, it sets the thread's priority to a cooperative level and checks the preempt status again, ensuring each state transition is as expected.
 * @param {void} *p1, *p2, *p3 are unused parameters, included for compatibility with the thread function signature.
 * @return {None}
 */
static void tpreempt_ctx(void *p1, void *p2, void *p3)
{
    if (k_is_preempt_thread()) {
        printk("TESTPOINT PASSED: The thread's priority is in the preemptible range.\n");
    } else {
        printk("TESTPOINT FAILED: The thread's priority is not in the preemptible range.\n");
    }

    k_sched_lock();
    if (k_is_preempt_thread()) {
        printk("TESTPOINT FAILED: The thread should not be preemptible after locking the scheduler.\n");
    } else {
        printk("TESTPOINT PASSED: The thread has locked the scheduler.\n");
    }
    k_sched_unlock();
    if (k_is_preempt_thread()) {
        printk("TESTPOINT PASSED: The thread has unlocked the scheduler and is preemptible again.\n");
    } else {
        printk("TESTPOINT FAILED: The thread is still non-preemptible after unlocking the scheduler.\n");
    }

    k_thread_priority_set(k_current_get(), K_PRIO_COOP(1));
    if (k_is_preempt_thread()) {
        printk("TESTPOINT FAILED: The thread's priority should be in the cooperative range now.\n");
    } else {
        printk("TESTPOINT PASSED: The thread's priority is in the cooperative range.\n");
    }
    k_sem_give(&end_sema);
}

/**
 * @name: tcoop_ctx
 * @msg:  This function evaluates the behavior of a cooperative thread in relation to scheduler locking and thread priority adjustments. It begins by verifying that the current thread is non-preemptible due to its cooperative nature. The function then adjusts the thread's priority to a preemptive level and re-evaluates its preempt status. Following this, it locks and unlocks the scheduler, each time checking the thread's preempt status to confirm that the operations are effectively altering the thread's preemptibility as expected.
 * @param {void} *p1, *p2, *p3 are placeholders for potential future parameters, serving no purpose in the current context.
 * @return {None}
 */
static void tcoop_ctx(void *p1, void *p2, void *p3)
{
    if (k_is_preempt_thread()) {
        printk("TESTPOINT FAILED: The thread's priority should be in the cooperative range.\n");
    } else {
        printk("TESTPOINT PASSED: The thread's priority is in the cooperative range.\n");
    }

    k_thread_priority_set(k_current_get(), K_PRIO_PREEMPT(1));
    if (k_is_preempt_thread()) {
        printk("TESTPOINT PASSED: The thread's priority is now in the preemptible range.\n");
    } else {
        printk("TESTPOINT FAILED: The thread's priority is not in the preemptible range after adjustment.\n");
    }

    k_sched_lock();
    if (k_is_preempt_thread()) {
        printk("TESTPOINT FAILED: The thread should not be preemptible after locking the scheduler.\n");
    } else {
        printk("TESTPOINT PASSED: The thread has locked the scheduler.\n");
    }
    k_sched_unlock();
    if (k_is_preempt_thread()) {
        printk("TESTPOINT PASSED: The thread has unlocked the scheduler and is preemptible again.\n");
    } else {
        printk("TESTPOINT FAILED: The thread is still non-preemptible after unlocking the scheduler.\n");
    }

    k_sem_give(&end_sema);
}



/**
 * @brief Validate the correctness of k_is_preempt_thread()
 *
 * @details Create a preemptive thread, lock the scheduler
 * and call k_is_preempt_thread(). Unlock the scheduler and
 * call k_is_preempt_thread() again. Create a cooperative
 * thread and lock the scheduler k_is_preempt_thread() and
 * unlock the scheduler and call k_is_preempt_thread().
 *
 * @see k_is_preempt_thread()
 *
 * @ingroup kernel_sched_tests
 */
 void test_sched_is_preempt_thread(void)
{
	k_sem_init(&end_sema, 0, 1);

	/* create preempt thread */
	k_tid_t tid = k_thread_create(&tdata, tstack, STACK_SIZE,
				      tpreempt_ctx, NULL, NULL, NULL,
				      K_PRIO_PREEMPT(1), 0, K_NO_WAIT);
	k_sem_take(&end_sema, K_FOREVER);
	k_thread_abort(tid);

	/* create coop thread */
	tid = k_thread_create(&tdata, tstack, STACK_SIZE,
			      tcoop_ctx, NULL, NULL, NULL,
			      K_PRIO_COOP(1), 0, K_NO_WAIT);
	k_sem_take(&end_sema, K_FOREVER);
	k_thread_abort(tid);

}

