# 调度测试

## 1. 例程介绍

### test_priority_scheduling.c

本源代码演示和测试Zephyr 中基于优先级的抢占式调度行为。这种测试对于验证RTOS调度器在处理不同优先级的线程时的行为非常重要，确保更高优先级的线程能够按预期抢占较低优先级线程的执行。这段代码特别设计用于观察和验证多个线程按照其优先级被调度和执行的顺序。

### test_sched_is_preempt_thread.c

本源代码文件 `test_sched_is_preempt_thread.c` 是专为展示和测试 Zephyr 实时操作系统 (RTOS) 中基于优先级的抢占式调度行为而设计。通过创建抢占式和合作式线程，然后锁定和解锁调度器，以及调整线程的优先级，该测试验证了 `k_is_preempt_thread()` 函数的正确性。这一过程对于确认RTOS调度器能够正确处理不同优先级的线程，确保更高优先级的线程能够按预期抢占较低优先级线程的执行至关重要。本代码通过两个关键函数——`tpreempt_ctx` 和 `tcoop_ctx` ——分别检验了抢占式和合作式线程在调度器被锁定和解锁时的行为，以及在调整线程优先级后的状态变化，从而展现了线程按其优先级被调度和执行的顺序。

### test_sched_priority.c

本源代码文件是为了展示和测试Zephyr实时操作系统（RTOS）中基于优先级的抢占式调度行为而精心设计的。通过创建不同优先级的线程，并在特定条件下执行它们，这一系列测试验证了Zephyr调度器如何根据优先级和等待时间来调度线程。这些测试对于确保RTOS调度器能够正确地处理不同优先级的线程，保证更高优先级的线程可以按预期抢占较低优先级线程的执行非常关键。

- `test_priority_cooperative` 函数验证合作线程不会被抢占的功能。通过创建一个比当前线程优先级更高的合作线程，并验证该高优先级线程不会抢占当前较低优先级的合作线程。这一测试强调了合作线程（cooperative threads）之间的非抢占性，展示了即使在优先级较高的情况下，合作线程也不会打断其他合作线程的执行。
- `test_priority_preemptible` 函数演示了抢占式线程的抢占性特点。首先，它通过创建一个优先级低于当前线程的抢占式线程，验证新创建的线程不会抢占当前线程。随后，通过创建一个优先级高于当前线程的抢占式线程，确保新创建的线程可以抢占当前线程。这一测试突出了抢占式线程（preemptive threads）根据优先级的抢占机制，即高优先级的抢占式线程可以打断低优先级线程的执行。
- `test_priority_preemptible_wait_prio` 函数验证了在存在启动延迟时，抢占式线程的调度顺序。它通过创建四个优先级高于当前线程的抢占式线程，这些线程具有不同的启动延迟，来确保优先级最高且等待时间最长的线程首先被调度执行。此测试验证了调度器不仅考虑线程的优先级，还考虑了它们的等待时间，在多个线程具有相同优先级时，等待时间更长的线程将先获得执行机会。

### test_sched_timeslice_and_lock.c

本源代码文件是为了展示和测试Zephyr实时操作系统（RTOS）中的多线程调度行为而设计的。通过一系列精心设计的测试案例，该文件验证了Zephyr调度器在处理合作式（cooperative）和抢占式（preemptive）线程时的行为。特别地，这些测试案例涵盖了线程的创建、睡眠、唤醒、优先级调整、调度锁定和解锁，以及时间片调度等方面。这些测试对于确保RTOS调度器能够正确地管理不同优先级的线程，保证更高优先级的线程能够按预期抢占较低优先级线程的执行至关重要。

- `test_yield_cooperative` 验证了合作式线程在执行 `k_yield()`后的行为。该测试通过创建三个不同优先级的线程，并使当前合作式线程让出CPU，来检查更高优先级的线程是否如预期那样被执行。
- `test_sleep_cooperative` 检验了合作式线程调用 `k_sleep()`后的行为。该测试创建三个线程，并通过让当前线程睡眠，来验证所有就绪状态的线程是否都能够被正确地执行。
- `test_busy_wait_cooperative` 验证了合作式线程在执行 `k_busy_wait()`期间的行为。这个测试确保了在当前线程繁忙等待时，没有其他线程被执行。
- `test_sleep_wakeup_preemptible` 检验了使用 `k_wakeup()`唤醒处于睡眠状态的抢占式线程的行为。通过让一个线程睡眠然后被唤醒，该测试验证了唤醒操作能够按预期工作。
- `test_pending_thread_wakeup` 验证了 `k_wakeup()`对于等待中（pending）的线程的行为。该测试确保了 `k_wakeup()`在线程等待资源时不会错误地将其唤醒。
- `test_time_slicing_preemptible` 演示了在启用时间片（time slicing）的情况下抢占式线程的行为。这个测试验证了具有相同优先级的线程是否能够在各自的时间片内被执行。
- `test_time_slicing_disable_preemptible` 验证了在执行 `k_busy_wait()`时时间片调度被禁用的情况。该测试确保了在忙等待期间，具有相同优先级的其他线程不会被执行。
- `test_lock_preemptible` 通过锁定调度器，来验证在调度器锁定期间，即使创建了新的线程，这些线程也不会被执行的行为。
- `test_unlock_preemptible` 检验了调度器从锁定状态解锁后，之前创建的线程是否能够如预期执行。
- `test_unlock_nested_sched_lock` 通过测试嵌套的调度器锁定和解锁，来验证解锁操作是否正确地使线程恢复执行。
- `test_wakeup_expired_timer_thread` 验证了对于因定时器超时而被唤醒的线程，`k_wakeup()`函数的行为是否符合预期。

### test_sched_timeslice_reset.c

本源代码文件 `test_slice_reset.c` 是专为展示和测试Zephyr实时操作系统（RTOS）中基于时间片的抢占式线程调度行为而设计的。该文件通过 `test_slice_reset`函数，验证了当时间片被启用和禁用时，抢占式线程的行为。通过创建多个具有不同优先级和相同优先级的抢占式线程，并启用时间片，该测试确保每个线程都被赋予了预期的时间片周期来执行。

`test_slice_reset`函数执行以下关键步骤来完成其验证功能：

1. **禁用时间片**：首先，禁用时间片以确保在测试设置期间，时间片不会影响线程调度。
2. **创建线程**：随后，创建多个抢占式线程，它们具有不同的优先级。其中一部分线程被设置为具有相同的优先级，以验证相同优先级的线程在时间片启用时的行为。
3. **启用时间片**：通过调用 `k_sched_time_slice_set`函数，启用时间片，并设置时间片的大小。这是为了测试系统是否能够根据设定的时间片大小，正确地分配CPU时间给每个线程。
4. **执行和验证**：每个线程在其执行函数中计算它实际获得的CPU时间，并与预期的时间片大小进行比较。这一步骤验证了时间片是否被正确地应用于线程调度，以及线程是否在它们的时间片结束时被正确地抢占。
5. **禁用时间片和恢复环境**：在测试完成后，时间片被禁用，并恢复测试前的线程优先级，以保证测试的隔离性和重复性。

### test_slice_scheduling.c

这两个测试案例专门设计用于验证Zephyr实时操作系统（RTOS）中抢占式线程的时间片调度行为。这些测试对确保系统能够正确地按照预设的时间片分配CPU时间给不同的线程至关重要。

- test_slice_scheduling

**功能验证：** `test_slice_scheduling` 函数验证了当时间片（time slice）被启用时，拥有相同优先级的多个抢占式线程的调度行为。该测试通过创建具有相同优先级的多个线程，然后启用时间片调度，以确保每个线程均被赋予相等的执行时间。

**操作步骤：**

1. **禁用时间片：** 首先，通过设置时间片大小为0来禁用时间片，确保开始测试时时间片调度不会影响线程的执行。
2. **创建线程：** 创建具有相同优先级的多个抢占式线程。
3. **启用时间片：** 通过 `k_sched_time_slice_set`函数启用时间片，并设置适当的时间片大小。
4. **执行验证：** 主线程让出CPU，允许创建的线程按时间片轮转执行。每个线程在其分配的时间片内执行，并通过信号量同步，确保它们按预期接受CPU时间。
5. **结果验证：** 验证每个线程是否在其分配的时间片内完成执行，并且在每次迭代中，所有线程均有机会执行。

- test_slice_perthread

**功能验证：** `test_slice_perthread` 函数特别针对支持每个线程独立设置时间片大小的配置（`CONFIG_TIMESLICE_PER_THREAD`）进行测试。它验证了可以为单个线程设置特定的时间片大小，并确保这个线程在其时间片到期时正确地被抢占。

## 2. 如何使用例程

本例程需要以下硬件，

- E2000D/Q Demo板

### 2.1 硬件配置方法

保障串口正常工作后，不需要额外配置硬件

### 2.2 SDK配置方法

- 本例子已经提供好具体的编译指令，以下进行介绍：

1. ``west build -b e2000q_demo ./ -DOVERLAY_CONFIG=prj.conf``，编译命令， 使用west工具构建当前目录下的Zephyr项目，指定目标板为e2000q_demo,并使用prj.conf配置文件覆盖默认配置 ，最终生成的执行文件将会保存在./build/zephyr/zephyr.elf
2. ``west build -t clean``， 清除缓存 ，使用west工具的clean目标清理Zephyr构建系统可能生成的任何其他临时文件或缓存

### 2.3 构建和下载

- 编译例程

``west build -b e2000q_demo ./ -DOVERLAY_CONFIG=prj.conf``

- 编译主机测侧设置重启tftp服务器

```
sudo service tftpd-hpa restart
```

- 开发板侧使用bootelf命令跳转

```
setenv ipaddr 192.168.4.20  
setenv serverip 192.168.4.50 
setenv gatewayip 192.168.4.1 
tftpboot 0x90100000 zephyr.elf
bootelf -p 0x90100000
```

### 2.4 输出与实验现象

- 所有用例均提供一系列可变配置，可在例程全局变量中修改

#### 2.4.1 测试优先级调度

```
schedule_test priority_scheduling
```

![priority_scheduling_result](figs/README/1713405703335.png)

#### 2.4.2 测试当前线程是否可抢占

```
schedule_test is_preempt_thread
```

![is_preempt_thread_result](figs/README/1713405732059.png)

#### 2.4.3 通过优先级测试线程调度

```
schedule_test priority
```

![priority_result](figs/README/1713405767652.png)

#### 2.4.4 测试时间片重置

```
schedule_test timeslice_reset
```

![timeslice_reset_result](figs/README/1713405796614.png)

#### 2.4.5 测试时间片调度

```
schedule_test slice_scheduling
```

![slice_scheduling_result](figs/README/1713405833265.png)

#### 2.4.6 测试时间片和锁

```
schedule_test timeslice_lock
```

![timeslice_lock_result](figs/README/1713405866527.png)


## 3. 如何解决问题



## 4. 修改历史记录
