// Phytium is pleased to support the open source community by making Zephyr-SDK available.

//
// Copyright (C) 2024 Phytium Technology Co., Ltd. All rights reserved.
//
// Licensed under the Apache-2.0 License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// https://opensource.org/license/apache-2-0
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.
#include <zephyr/logging/log.h>
#include <zephyr/shell/shell.h>
#include "test_interrupt.h"
LOG_MODULE_REGISTER(app);

int main(void)
{
	return 0;
}


static int cmd_dynamic_isr(const struct shell *sh,
			      size_t argc, char **argv)
{
    test_isr_dynamic() ;
    return 0 ;
}

static int cmd_dynamic_shared_irq(const struct shell *sh,
			      size_t argc, char **argv)
{
    dynamic_shared_irq() ;
    return 0 ;
}

static int cmd_nested_irq(const struct shell *sh,
			      size_t argc, char **argv)
{
    test_nested_isr() ;
    return 0 ;
}

static int cmd_prevent_irq(const struct shell *sh,
			      size_t argc, char **argv)
{
    test_prevent_interruption() ;
    return 0 ;
}


static int cmd_static_shared_irq(const struct shell *sh,
			      size_t argc, char **argv)
{
    test_static_shared_irq_write() ;
    return 0 ;
}

SHELL_STATIC_SUBCMD_SET_CREATE(sub_isr_tests,
    SHELL_CMD(dynamic_isr, NULL, "Test dynamic ISR.", cmd_dynamic_isr),
    SHELL_CMD(dynamic_shared_irq, NULL, "Test dynamic shared IRQ.", cmd_dynamic_shared_irq),
    SHELL_CMD(nested_irq, NULL, "Test nested ISR.", cmd_nested_irq),
    SHELL_CMD(prevent_irq, NULL, "Test prevention of interruption.", cmd_prevent_irq),
    SHELL_CMD(static_shared_irq, NULL, "Test static shared IRQ write.", cmd_static_shared_irq),
    SHELL_SUBCMD_SET_END
);

/* Register 'isr_test' as a subcommand of 'kernel' */
SHELL_CMD_REGISTER(isr_test, &sub_isr_tests, "ISR testing commands", NULL);


