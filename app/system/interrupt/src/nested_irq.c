/*
 * Copyright (c) 2020 Stephanos Ioannidis <root@stephanos.io>
 * Copyright (c) 2018 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <zephyr/kernel.h>
#include "interrupt_util.h"

/*
 * Run the nested interrupt test for the supported platforms only.
 */
#if defined(CONFIG_CPU_CORTEX_M) || defined(CONFIG_ARC) || \
	defined(CONFIG_GIC)
#define TEST_NESTED_ISR
#endif

#define DURATION	5

#define ISR0_TOKEN	0xDEADBEEF
#define ISR1_TOKEN	0xCAFEBABE

/*
 * This test uses two IRQ lines selected within the range of available IRQs on
 * the target SoC.  These IRQs are platform and interrupt controller-specific,
 * and must be specified for every supported platform.
 *
 * In terms of priority, the IRQ1 is triggered from the ISR of the IRQ0;
 * therefore, the priority of IRQ1 must be greater than that of the IRQ0.
 */

/*
 * For the platforms that use the ARM GIC, use the SGI (software generated
 * interrupt) lines 13 and 14 for testing.
 */
#define IRQ0_LINE	14
#define IRQ1_LINE	13

/*
 * Choose lower prio for IRQ0 and higher priority for IRQ1
 * Minimum legal value of GICC BPR is '3' ie  <gggg.ssss>
 * Hence choosing default priority and highest possible priority
 * '0x0' as the priorities so that the preemption rule applies
 * generically to all GIC versions and security states.
 */
#define IRQ0_PRIO	IRQ_DEFAULT_PRIORITY
#define IRQ1_PRIO	0x0



static uint32_t irq_line_0;
static uint32_t irq_line_1;

static uint32_t isr0_result;
static uint32_t isr1_result;

void isr1(const void *param)
{
	ARG_UNUSED(param);

	printk("isr1: Enter\n");

	/* Set verification token */
	isr1_result = ISR1_TOKEN;

	printk("isr1: Leave\n");
}

void isr0(const void *param)
{
	ARG_UNUSED(param);

	printk("isr0: Enter\n");

	/* Set verification token */
	isr0_result = ISR0_TOKEN;

	/* Trigger nested IRQ 1 */
	trigger_irq(irq_line_1);

	/* Wait for interrupt */
	k_busy_wait(MS_TO_US(DURATION));

	/* Validate nested ISR result token */
	if (isr1_result != ISR1_TOKEN) 
	{
        printk("ERROR: isr1 did not execute as expected. Expected %d, got %d\n", ISR1_TOKEN, isr1_result);
    } else {
        printk("SUCCESS: isr1 executed as expected.\n");
    }

	printk("isr0: Leave\n");
}

/**
 * @brief Test interrupt nesting
 *
 * @ingroup kernel_interrupt_tests
 *
 * This routine tests the interrupt nesting feature, which allows an ISR to be
 * preempted in mid-execution if a higher priority interrupt is signaled. The
 * lower priority ISR resumes execution once the higher priority ISR has
 * completed its processing.
 *
 * The expected control flow for this test is as follows:
 *
 * 1. [thread] Trigger IRQ 0 (lower priority)
 * 2. [isr0] Set ISR 0 result token and trigger IRQ 1 (higher priority)
 * 3. [isr1] Set ISR 1 result token and return
 * 4. [isr0] Validate ISR 1 result token and return
 * 5. [thread] Validate ISR 0 result token
 */
void test_nested_isr(void)
{
	/* Resolve test IRQ line numbers */

	irq_line_0 = IRQ0_LINE;
	irq_line_1 = IRQ1_LINE;


	/* Connect and enable test IRQs */
	arch_irq_connect_dynamic(irq_line_0, IRQ0_PRIO, isr0, NULL, 0);
	arch_irq_connect_dynamic(irq_line_1, IRQ1_PRIO, isr1, NULL, 0);

	irq_enable(irq_line_0);
	irq_enable(irq_line_1);

	/* Trigger test IRQ 0 */
	trigger_irq(irq_line_0);

	/* Wait for interrupt */
	k_busy_wait(MS_TO_US(DURATION));

	/* Validate ISR result token */
	if (isr0_result != ISR0_TOKEN) 
	{
        printk("ERROR: isr0 did not execute as expected. Expected token: %d, Got token: %d\n", ISR0_TOKEN, isr0_result);
    } else {
        printk("SUCCESS: isr0 executed as expected with correct token.\n");
    }
}
