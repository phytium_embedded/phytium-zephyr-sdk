#ifndef __TEST_INTERRUPT_H__
#define __TEST_INTERRUPT_H__



void test_isr_dynamic(void) ;


void dynamic_shared_irq(void) ;


void test_nested_isr(void) ;


void test_prevent_interruption(void) ;


void test_isr_regular(void) ;


void test_static_shared_irq_write(void) ;

#endif /* __TEST_INTERRUPT_H__ */
