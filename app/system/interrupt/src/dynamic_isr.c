/*
 * Copyright (c) 2018 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <zephyr/kernel.h>
#include "interrupt_util.h"

#define ISR_DYN_ARG	0xab249cfd

static unsigned int handler_has_run;
static uintptr_t handler_test_result;

static void dyn_isr(const void *arg)
{
	ARG_UNUSED(arg);
	handler_test_result = (uintptr_t)arg;
	handler_has_run++;
}


extern struct _isr_table_entry _sw_isr_table[];

/**
 * @brief Test dynamic ISR installation
 *
 * @ingroup kernel_interrupt_tests
 *
 * @details This routine locates an unused entry in the software ISR table,
 * installs a dynamic ISR to the unused entry by calling the dynamic
 * configured function, and verifies that the ISR is successfully installed
 * by checking the software ISR table entry.
 *
 * @see arch_irq_connect_dynamic()
 */
void test_isr_dynamic(void)
{
	int i;
	const void *argval;

	for (i = 0; i < (CONFIG_NUM_IRQS - CONFIG_GEN_IRQ_START_VECTOR); i++) {
		if (_sw_isr_table[i].isr == z_irq_spurious) {
			break;
		}
	}

	if (!(_sw_isr_table[i].isr == z_irq_spurious)) {
        printk("ERROR: Could not find slot for dynamic ISR.\n");
    } else {
        printk("SUCCESS: Found slot for dynamic ISR at %d.\n", CONFIG_GEN_IRQ_START_VECTOR + i);
    }

	printk("installing dynamic ISR for IRQ %d\n",
	       CONFIG_GEN_IRQ_START_VECTOR + i);

	argval = (const void *)&i;
	arch_irq_connect_dynamic(i + CONFIG_GEN_IRQ_START_VECTOR, 0, dyn_isr,
				 argval, 0);

	if (!(_sw_isr_table[i].isr == dyn_isr && _sw_isr_table[i].arg == argval)) {
        printk("ERROR: Dynamic ISR did not install successfully.\n");
    } else {
        printk("SUCCESS: Dynamic ISR installed successfully for IRQ %d.\n", CONFIG_GEN_IRQ_START_VECTOR + i);
    }
}
