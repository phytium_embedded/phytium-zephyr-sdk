# SMP 测试

## 1. 例程介绍

以下是对指定函数的详细描述，基于Zephyr RTOS的SMP（对称多处理）功能的测试场景。

- smp_tests_setup
  本函数用于SMP测试的初始化。通过让系统稍微睡眠，确保所有CPU都进入空闲线程，从而可以正确退出并运行主测试线程。这是SMP测试的通用准备步骤，确保测试环境的一致性和稳定性。
- test_coop_resched_threads
  验证合作（cooperative）线程在SMP环境中的非抢占性。通过在多核上创建与CPU核数相等的合作线程，确保最后一个创建的线程不会抢占任何已经在运行的线程。这个测试验证了合作线程即使在多核环境下也保持其非抢占特性。
- test_coop_switch_in_abort
  测试在SMP系统中中止（abort）线程时的合作线程切换行为。特别是，在一个线程中调用 `k_thread_abort()`时，验证是否可以正确地切换到其他合作线程执行，这是检测中止线程时上下文切换正确性的重要测试。
- test_cpu_id_threads
  验证SMP环境中线程可以正确识别其运行的CPU核心。通过在一个线程中获取当前CPU的ID，然后在另一个线程中验证这个ID，以确保线程的CPU亲和性（affinity）和调度行为符合预期。
- test_fatal_on_smp
  测试在SMP环境中触发致命错误（fatal error）的行为。验证当一个线程因致命错误被终止时，系统能否正确处理并确保系统稳定性。这是评估系统错误处理机制在多核环境下正确性的关键测试。
- test_get_cpu
  验证在SMP环境中能否正确获取当前线程运行的CPU。通过在不同线程中调用获取CPU ID的函数，并比较结果，以确保线程能够准确识别其运行的处理器。
- test_inc_concurrency
  测试SMP环境下的并发性。通过创建多个线程同时增加一个全局计数器，验证是否所有线程都能正确执行并发操作，以及全局状态的一致性和正确性。
- test_preempt_resched_threads
  在SMP环境中验证抢占式（preemptive）线程的抢占性。通过创建不同优先级的抢占式线程，验证更高优先级的线程能否正确抢占CPU资源。
- test_sleep_threads
  测试在SMP环境中线程睡眠（sleep）的行为。通过让线程睡眠，然后验证是否所有线程在预定时间后都能被正确唤醒和执行，以评估睡眠调度机制的正确性。
- test_smp_coop_threads
  验证在SMP环境下，多个合作线程能否同时在不同核心上运行。这个测试通过创建多个合作线程并观察它们是否能够被分配到不同的CPU上执行，来评估系统的多核调度能力。
- test_smp_ipi
  测试SMP环境中的内核间中断（Inter-Processor Interrupts, IPI）功能。通过触发IPI并验证是否所有CPU核心都能接收到IPI信号，来测试IPI的正确性和效率。
- test_smp_release_global_lock
  验证在SMP系统中释放全局锁的行为。这个测试专注于全局锁在上下文切换和线程中止时的正确释放，是验证SMP系统同步机制正确性的重要测试。
- test_smp_switch_torture
  对SMP系统中的上下文切换代码进行压力测试。通过频繁地在所有CPU核心上请求线程切换，测试系统的稳定

性和上下文切换的性能。

- test_wakeup_threads
  测试SMP环境中线程唤醒（wakeup）的行为。通过创建多个线程并使它们睡眠，然后从主线程中唤醒这些线程，以验证唤醒机制的正确性和效率。
- test_workq_on_smp
  验证SMP环境下系统工作队列（workqueue）的行为。通过提交工作项到系统工作队列，并确保工作项能在不同的CPU核心上执行，来测试工作队列的多核调度能力。
- test_yield_threads
  在SMP环境中测试线程主动让出（yield）CPU的行为。通过创建多个线程并使用 `k_yield()`让出CPU，然后验证是否所有线程都能按预期被调度执行，来评估调度器的公平性和响应性。

## 2. 如何使用例程

本例程需要以下硬件，

- E2000D/Q Demo板

### 2.1 硬件配置方法

保障串口正常工作后，不需要额外配置硬件

### 2.2 SDK配置方法

- 本例子已经提供好具体的编译指令，以下进行介绍：

1. ``west build -b e2000q_demo_smp ./ -DOVERLAY_CONFIG=prj.conf``，编译命令， 使用west工具构建当前目录下的Zephyr项目，指定目标板为e2000q_demo_smp,并使用prj.conf配置文件覆盖默认配置 ，最终生成的执行文件将会保存在./build/zephyr/zephyr.elf
2. ``west build -t clean``， 清除缓存 ，使用west工具的clean目标清理Zephyr构建系统可能生成的任何其他临时文件或缓存

### 2.3 构建和下载

- 编译例程

``west build -b e2000q_demo_smp ./ -DOVERLAY_CONFIG=prj.conf``

- 编译主机测侧设置重启tftp服务器

```
sudo service tftpd-hpa restart
```

- 开发板侧使用bootelf命令跳转

```
setenv ipaddr 192.168.4.20  
setenv serverip 192.168.4.50 
setenv gatewayip 192.168.4.1 
tftpboot 0x90100000 zephyr.elf
bootelf -p 0x90100000
```

### 2.4 输出与实验现象

- 所有用例均提供一系列可变配置，可在例程全局变量中修改

### 2.4.1 smp 测试

```
smp_test
```

![1713407267028](figs/README/1713407267028.png)

## 3. 如何解决问题

1. 本例程仅可以使用开启smp的配置

## 4. 修改历史记录
