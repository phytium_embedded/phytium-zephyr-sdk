// Phytium is pleased to support the open source community by making Zephyr-SDK available.

//
// Copyright (C) 2024 Phytium Technology Co., Ltd. All rights reserved.
//
// Licensed under the Apache-2.0 License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// https://opensource.org/license/apache-2-0
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#include <zephyr/kernel.h>
#include <zephyr/sd/sdmmc.h>
#include <zephyr/device.h>
#include <zephyr/drivers/disk.h>
#include <zephyr/logging/log.h>
#include <zephyr/shell/shell.h>

LOG_MODULE_REGISTER(main);

#define MB_SIZE                (1024*1024)
#define SD_TOTAL_MB             1
#define SD_TOTAL_MB_SIZE        (SD_TOTAL_MB*MB_SIZE)
#define SD_BLOCK_SIZE          512UL /* most tested sd memory cards have 512 block size by default */

#define WRITE_BLOCKS 1024 // 这里假设每次写入40块，即20KB
#define TOTAL_BLOCKS SD_TOTAL_MB_SIZE / SD_BLOCK_SIZE // 总共要写入20MB数据

uint8_t write_buf[SD_BLOCK_SIZE * WRITE_BLOCKS] __attribute__((aligned(CONFIG_SDHC_BUFFER_ALIGNMENT))) ;
uint8_t read_buf[SD_BLOCK_SIZE * WRITE_BLOCKS]  __attribute__((aligned(CONFIG_SDHC_BUFFER_ALIGNMENT))) ;


// 函数用于打印进度条
void print_progress(int current, int total) {
    int progress = current * 100 / total;
    printf("\rProgress: %d%% [", progress);
    for (int i = 0; i < 100; i += 10) {
        if (i < progress) {
            printf("=");
        } else if (i == progress) {
            printf(">");
        } else {
            printf(" ");
        }
    }
    printf("]");
    fflush(stdout); // 确保即时打印到控制台
}


void sd_perf_test(const struct shell *shell, size_t argc, char **argv)
{
    struct sd_card card;
    int ret;
    const struct device *const sdhc_dev = DEVICE_DT_GET(DT_NODELABEL(sdhc1));
    uint32_t sector_size;
    uint32_t sector_count;
    int64_t start_time, end_time;
    double write_duration, read_duration, write_speed, read_speed;
    
    if(!device_is_ready(sdhc_dev))
	{
		LOG_ERR("Sd device is not ready");
	}

	ret = sd_is_card_present(sdhc_dev);
	if(!ret)
	{
		LOG_ERR("SD card not present in slot");
	}

	ret = sd_init(sdhc_dev, &card);
	if(ret)
	{
		LOG_ERR("SD card sd_init is not ok \r\n");
	}

	ret = sdmmc_ioctl(&card, DISK_IOCTL_GET_SECTOR_COUNT, &sector_count);
	printf("SD card reports sector count of %d\n", sector_count);

	ret = sdmmc_ioctl(&card, DISK_IOCTL_GET_SECTOR_SIZE, &sector_size);
	printf("SD card reports sector size of %d\n", sector_size);

    // 初始化write_buf
    for (int i = 0; i < sizeof(write_buf); ++i) {
        write_buf[i] = i % 256;
    }

    // 写入数据
    printf("Writing data...\n");
    start_time = k_uptime_get();
    for (int i = 0; i < TOTAL_BLOCKS / WRITE_BLOCKS; ++i) 
    {
        if (sdmmc_write_blocks(&card, write_buf, i * WRITE_BLOCKS, WRITE_BLOCKS) != 0) {
            printf("Write failed at block %d\n", i * WRITE_BLOCKS);
            return;
        }
        print_progress(i + 1, TOTAL_BLOCKS / WRITE_BLOCKS); 
    }
    end_time = k_uptime_get();
    printf("\nWrite completed.\n");
    write_duration = (end_time - start_time) / 1000.0; // 将毫秒转换为秒
    printf("\nWrite completed in %.2f seconds.\n", write_duration);
    write_speed = SD_TOTAL_MB / write_duration; // 计算写速度，单位MB/s
    printf("\nWrite completed in %.2f seconds at %.2f MB/s.\n", write_duration, write_speed);
    
    // 读取并验证数据
    printf("Reading data...\n");
    start_time = k_uptime_get();
    for (int i = 0; i < TOTAL_BLOCKS / WRITE_BLOCKS; ++i)
    {
        if (sdmmc_read_blocks(&card, read_buf, i * WRITE_BLOCKS, WRITE_BLOCKS) != 0) {
            printf("Read failed at block %d\n", i * WRITE_BLOCKS);
            return;
        }

        print_progress(i + 1, TOTAL_BLOCKS / WRITE_BLOCKS); 
    }
    end_time = k_uptime_get();
    read_duration = (end_time - start_time) / 1000.0; // 将毫秒转换为秒
    printf("\nRead completed in %.2f seconds.\n", read_duration);
    read_speed = SD_TOTAL_MB / read_duration; // 计算读速度，单位MB/s
    printf("\nRead completed in %.2f seconds at %.2f MB/s.\n", read_duration, read_speed);


    for (int i = 0; i < TOTAL_BLOCKS / WRITE_BLOCKS; ++i)
    {
        memset(read_buf, 0, sizeof(read_buf));
        if (sdmmc_read_blocks(&card, read_buf, i * WRITE_BLOCKS, WRITE_BLOCKS) != 0) {
            printf("Read failed at block %d\n", i * WRITE_BLOCKS);
            return;
        }

        if (memcmp(write_buf, read_buf, SD_BLOCK_SIZE * WRITE_BLOCKS) != 0) {
            printf("Data mismatch detected at block %d\n", i * WRITE_BLOCKS);
            return;
        }
    }


    printf("\n SDHC performance test completed successfully.\n");
    
    return  ;
}


SHELL_CMD_ARG_REGISTER(sdtest, NULL, "Start SD card performance test", sd_perf_test, 1, 0);

int main(void)
{
	return 0 ; 
}
