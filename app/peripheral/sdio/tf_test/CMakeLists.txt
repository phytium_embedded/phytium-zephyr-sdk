# SPDX-License-Identifier: Apache-2.0

cmake_minimum_required(VERSION 3.20.0)

set(EXTRA_ZEPHYR_MODULES "$ENV{ZEPHYR_BASE}/../modules/hal/phytium")
find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})
project(tf_test)

FILE(GLOB app_sources src/*.c)
target_sources(app PRIVATE ${app_sources})
