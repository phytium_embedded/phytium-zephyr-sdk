# PCIE ECAM测试

## 1. 例程介绍

本例程用于测试zephyr原生PCIE ECAM驱动，设备树中pcie相关配置后，驱动会自动进行枚举操作

## 2. 如何使用例程

本例程需要以下硬件，

- E2000D/Q Demo板
- pcie功能性设备或桥设备（转接卡）

### 2.1 硬件配置方法

将pcie功能性设备或桥设备（转接卡）插入开发板对应插槽

### 2.2 SDK配置方法

- 本例程提供如下编译指令：

1. ``make boot``，编译命令， 使用west工具构建当前目录下的Zephyr项目，默认指定的目标板为e2000q_demo，可以在makefile文件中修改（如有修改，sample.yaml文件中的相关配置也需要一并修改），并使用prj.conf配置文件覆盖默认配置 ，最终生成的执行文件将会保存在./build/zephyr/zephyr.elf
2. ``make clean``， 清除缓存 ，使用west工具的clean目标清理Zephyr构建系统可能生成的任何其他临时文件或缓存

### 2.3 构建和下载

- 编译例程

``make boot``

- 编译主机测侧设置重启tftp服务器

```
sudo service tftpd-hpa restart
```

- 开发板侧使用bootelf命令跳转

```
setenv ipaddr 192.168.4.20  
setenv serverip 192.168.4.50 
setenv gatewayip 192.168.4.1 
tftpboot 0x90100000 zephyr.elf
bootelf -p 0x90100000
```

### 2.4 输出与实验现象

- 所有用例均提供一系列可变配置，可在例程全局变量中修改

### 2.4.1 PCIE ECAM枚举测试

进入系统后将出现如下打印，表示BAR空间的分配：

![pcie_bar_region](figs/README/pcie_bar_region.jpg)

输入```pcie```，将出现如下打印，表示枚举的结果：

![pcie_enum_result](figs/README/pcie_enum_result.jpg)

输入```pcie ls 4:0.0 dump```，将出现如下打印，表示单个pcie（BDF = 4:0.0）设备的配置空间与基本特性展示：

![pcie_ls_result](figs/README/pcie_ls_result.jpg)


## 3. 如何解决问题

## 4. 修改历史记录