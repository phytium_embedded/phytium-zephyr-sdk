// Phytium is pleased to support the open source community by making Zephyr-SDK available.

//
// Copyright (C) 2024 Phytium Technology Co., Ltd. All rights reserved.
//
// Licensed under the Apache-2.0 License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// https://opensource.org/license/apache-2-0
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/logging/log.h>
#include <zephyr/shell/shell.h>

LOG_MODULE_REGISTER(main);

int main(void)
{
    printf("This is PCIE ecam test example.\r\n");
	return 0 ; 
}