# net/echo卡测试

## 1. 例程介绍

函数 `main` 实现了一个简单的单线程TCP echo服务器。服务器监听指定的端口（这里是4242端口），接收来自客户端的连接，然后回显（echo）客户端发送的所有数据。这个过程包含以下关键步骤：

1. **创建套接字（socket）**：通过调用 `socket()` 函数创建一个TCP套接字，用于监听来自客户端的连接请求。
2. **绑定地址和端口**：将套接字与本地地址和指定的端口号绑定。这里使用的是 `INADDR_ANY`，表示服务器将接受指向所有本地接口的连接。
3. **监听端口**：通过调用 `listen()` 函数，使得服务器套接字进入监听状态，等待来自客户端的连接请求。
4. **等待并接受连接**：服务器进入一个无限循环，使用 `accept()` 函数等待并接受来自客户端的连接请求。一旦接受到一个连接，就为该连接创建一个新的套接字。
5. **处理客户端数据**：服务器通过新创建的套接字接收来自客户端的数据。接收到的数据将被回显给客户端，即服务器读取客户端发送的每一条消息，然后将相同的消息发送回客户端。
6. **关闭客户端连接**：当客户端断开连接或发送数据出现错误时，服务器将关闭与该客户端的连接，并准备接受下一个连接请求。

特别注意的是，此代码既可以在普通的POSIX兼容操作系统上运行（如Linux），也可以在Zephyr RTOS上运行，这得益于条件编译指令（`#ifndef __ZEPHYR__`）用于区分不同的环境所需的头文件和API调用。

## 2. 如何使用例程

本例程需要以下硬件，

- E2000D/Q Demo板

### 2.1 硬件配置方法

保障串口正常工作后，需要确认网络是否连接好

### 2.2 SDK配置方法

- 请根据自己的网络环境，修改prj.conf文件中的CONFIG_NET_CONFIG_MY_IPV4_ADDR 条目为您当前网络环境中分配给开发板的IP.
- 本例子已经提供好具体的编译指令，以下进行介绍：

1. ``west build -b e2000q_demo ./ -DOVERLAY_CONFIG=prj.conf``，编译命令， 使用west工具构建当前目录下的Zephyr项目，指定目标板为e2000q_demo,并使用prj.conf配置文件覆盖默认配置 ，最终生成的执行文件将会保存在./build/zephyr/zephyr.elf
2. ``west build -t clean``， 清除缓存 ，使用west工具的clean目标清理Zephyr构建系统可能生成的任何其他临时文件或缓存

### 2.3 构建和下载

- 编译例程

``west build -b e2000q_demo ./ -DOVERLAY_CONFIG=prj.conf``

- 编译主机测侧设置重启tftp服务器

```
sudo service tftpd-hpa restart
```

- 开发板侧使用bootelf命令跳转

```
setenv ipaddr 192.168.4.20  
setenv serverip 192.168.4.50 
setenv gatewayip 192.168.4.1 
tftpboot 0x90100000 zephyr.elf
bootelf -p 0x90100000
```

### 2.4 输出与实验现象

- 所有用例均提供一系列可变配置，可在例程全局变量中修改

#### 2.4.1 echo 例程使用

1. 将zephyr.elf 在开发板上加载起来之后会打印以下内容：

![1713412848289](figs/README/1713412848289.png)

2. 在PC主机上输入以下命令，则可以连接开发板echo服务器：

```
telnet 192.168.4.7 4242
```

3. 当PC主机连接成功之后，开发板上会打印以下内容：

![1713417340810](figs/README/1713417340810.png)

## 3. 如何解决问题

## 4. 修改历史记录
