# telnet测试

## 1. 例程介绍

本例程实现了网络接口的配置和初始化，支持多种网络配置方案，包括静态IPv4地址分配、DHCPv4客户端配置以及IPv6地址配置。这个函数是网络应用程序的入口点，目的是为了展示如何在网络设备上设置不同类型的网络地址。具体的操作步骤包括：

1. **获取默认网络接口**：通过调用 `net_if_get_default()` 函数获取系统默认的网络接口。

2. **配置IPv4地址**（如果启用了CONFIG_NET_IPV4且未启用CONFIG_NET_DHCPV4）：
   - 如果定义了静态IPv4地址（`CONFIG_NET_CONFIG_MY_IPV4_ADDR`），则调用 `setup_ipv4()` 函数配置该地址到网络接口上。
   - 该函数将静态IPv4地址添加到网络接口，并打印该地址到日志。

3. **启动DHCPv4客户端**（如果启用了CONFIG_NET_DHCPV4）：
   - 调用 `setup_dhcpv4()` 函数启动DHCPv4客户端，以便自动从DHCP服务器获取IPv4地址。
   - 该函数还注册了一个网络管理事件回调，以处理IPv4地址添加事件。当获得DHCP分配的IPv4地址时，它会打印出地址、租约时间、子网掩码和默认网关。

4. **配置IPv6地址**（如果启用了CONFIG_NET_IPV6）：
   - 调用 `setup_ipv6()` 函数配置静态IPv6地址（`CONFIG_NET_CONFIG_MY_IPV6_ADDR`）到网络接口上，并尝试添加一个IPv6组播地址（`MCAST_IP6ADDR`）。
   - 该函数将静态IPv6地址添加到网络接口，并打印该地址到日志。同时，也尝试添加一个IPv6组播地址并进行相应的处理。


## 2. 如何使用例程

本例程需要以下硬件，

- E2000D/Q Demo板

### 2.1 硬件配置方法

保障串口正常工作后，需要确认网络是否连接好

### 2.2 SDK配置方法

- 请根据自己的网络环境，修改prj.conf文件中的CONFIG_NET_CONFIG_MY_IPV4_ADDR与CONFIG_NET_CONFIG_MY_IPV6_ADDR 条目为您当前网络环境中分配给开发板的IP.

- 本例子已经提供好具体的编译指令，以下进行介绍：

1. ``west build -b e2000q_demo ./ -DOVERLAY_CONFIG=prj.conf``，编译命令， 使用west工具构建当前目录下的Zephyr项目，指定目标板为e2000q_demo,并使用prj.conf配置文件覆盖默认配置 ，最终生成的执行文件将会保存在./build/zephyr/zephyr.elf
2. ``west build -t clean``， 清除缓存 ，使用west工具的clean目标清理Zephyr构建系统可能生成的任何其他临时文件或缓存

### 2.3 构建和下载

- 编译例程

``west build -b e2000q_demo ./ -DOVERLAY_CONFIG=prj.conf``

- 编译主机测侧设置重启tftp服务器

```
sudo service tftpd-hpa restart
```

- 开发板侧使用bootelf命令跳转

```
setenv ipaddr 192.168.4.20  
setenv serverip 192.168.4.50 
setenv gatewayip 192.168.4.1 
tftpboot 0x90100000 zephyr.elf
bootelf -p 0x90100000
```

### 2.4 输出与实验现象

- 所有用例均提供一系列可变配置，可在例程全局变量中修改

#### 2.4.1 telnet 例程使用

1. 将zephyr.elf 在开发板上加载起来.

2. 在PC主机上输入以下命令，则可以连接开发板echo服务器：

```
telnet 192.168.4.7
```

3. 当PC主机连接成功之后，开发板上会打印以下内容：

![1713424173372](figs/README/1713424173372.png)

## 3. 如何解决问题

## 4. 修改历史记录
