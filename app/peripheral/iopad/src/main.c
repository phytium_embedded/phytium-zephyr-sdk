

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/logging/log.h>
#include <zephyr/shell/shell.h>
#include <zephyr/drivers/pinctrl.h>

#define UART1 DT_NODELABEL(pinctrl)

#define UART1_NODE DT_NODELABEL(uart1)

PINCTRL_DT_DEFINE(UART1_NODE);

void test_pinctrl_dt_define(void)
{
    // 获取配置结构体
    const struct pinctrl_dev_config *config = &Z_PINCTRL_DEV_CONFIG_NAME(UART1_NODE);
    // 打印配置结构体的信息
    printf("Pinctrl Dev Config for UART1:\n");
    printf("Number of states: %d\n", config->state_cnt);
    for (int i = 0; i < config->state_cnt; i++) {
        printf("State %d:\n", i);
        printf("  Pins: %d\n", config->states[i].pin_cnt);
        for (int j = 0; j < config->states[i].pin_cnt; j++) {
            printf("    Pin %d: reg_offset=0x%x, pin_func=%d\n",
                   j, config->states[i].pins[j].pin_reg_offset, config->states[i].pins[j].func_num);
        }
    }
    pinctrl_apply_state(&(*config), PINCTRL_STATE_DEFAULT);
}


int main(void)
{
    test_pinctrl_dt_define();
	return 0 ;
}

