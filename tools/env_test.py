#!/usr/bin/env python3
import subprocess

# 执行 "sudo apt update" 命令
update_result = subprocess.run(["sudo", "apt", "update"], capture_output=True)
if update_result.returncode == 0:
    print("Package lists updated successfully.")
else:
    print("Failed to update package lists.")
    print("Error message:", update_result.stderr.decode())

# 执行 "sudo apt upgrade" 命令
upgrade_result = subprocess.run(["sudo", "apt", "upgrade"], capture_output=True)
if upgrade_result.returncode == 0:
    print("Packages upgraded successfully.")
else:
    print("Failed to upgrade packages.")
    print("Error message:", upgrade_result.stderr.decode())

# 执行 "sudo bash ./tools/kitware-archive.sh" 命令
kitware_result = subprocess.run(["sudo", "bash", "./tools/kitware-archive.sh"], capture_output=True)
if kitware_result.returncode == 0:
    print("kitware-archive.sh script executed successfully.")
else:
    print("Failed to execute kitware-archive.sh script.")
    print("Error message:", kitware_result.stderr.decode())

# 执行必备程序安装命令
essential_packages = [
    "git", "cmake", "ninja-build", "gperf",
    "ccache", "dfu-util", "device-tree-compiler", "wget",
    "python3-dev", "python3-pip", "python3-setuptools", "python3-tk", "python3-wheel", "xz-utils", "file",
    "make", "gcc", "gcc-multilib", "g++-multilib", "libsdl2-dev", "libmagic1"
]
install_command = ["sudo", "apt", "install", "--no-install-recommends"] + essential_packages
install_result = subprocess.run(install_command, capture_output=True)
if install_result.returncode == 0:
    print("Essential packages installed successfully.")
else:
    print("Failed to install essential packages.")
    print("Error message:", install_result.stderr.decode())

# 检查安装是否成功
check_commands = ["cmake --version", "python3 --version", "dtc --version"]
for cmd in check_commands:
    check_result = subprocess.run(cmd, shell=True, capture_output=True)
    if check_result.returncode == 0:
        print(cmd, "executed successfully.")
    else:
        print("Failed to check installation of", cmd.split()[0])
        print("Error message:", check_result.stderr.decode())

# 安装python虚拟化环境
venv_install_result = subprocess.run(["sudo", "apt", "install", "python3-venv"], capture_output=True)
if venv_install_result.returncode == 0:
    print("python3-venv package installed successfully.")
else:
    print("Failed to install python3-venv package.")
    print("Error message:", venv_install_result.stderr.decode())

