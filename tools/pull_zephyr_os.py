#!/usr/bin/env python3
#!/usr/bin/env python
import subprocess
import os

# 执行 "git clone" 命令
git_clone_command = ["git", "clone", "-b", "phytium_v3.5.0", "https://gitee.com/phytium_embedded/zephyr_kernel.git","zephyr"]
init_result = subprocess.run(git_clone_command, capture_output=True, text=True)
if init_result.returncode == 0:
    print("Code pulled successfully.")
else:
    print("Failed to pull the code.")
    print("Error message:", init_result.stderr)
    print("Output message:", init_result.stdout)

origin_dir = os.getcwd()
os.chdir(f"{origin_dir}/zephyr")
os.system(f"git checkout 34f4373122b92232c36e20b08cd7dbf8ce9733d9")
os.chdir(origin_dir)