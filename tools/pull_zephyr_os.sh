#!/bin/bash

# 执行 "git clone" 命令
git clone -b phytium_v3.5.0 https://gitee.com/phytium_embedded/zephyr_kernel.git zephyr
cd zephyr
git checkout 34f4373122b92232c36e20b08cd7dbf8ce9733d9
cd ..

if [ $? -eq 0 ]; then
    echo "Code pulled successfully."
else
    echo "Failed to pull the code."
    echo "Error message:" $?
fi