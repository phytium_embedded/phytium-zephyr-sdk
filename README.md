# Phytium-Zephyr-SDK

**v1.0.0** [ReleaseNote](./doc/ChangeLog.md)

## 1. 仓库介绍

## 1.1 本仓库特色

此项目是一个开源软件，专为物联网领域设计，基于Zephyr实时操作系统，针对Phytium系列CPU进行了专门的适配和应用开发。我们的目标是降低开发者的使用门槛，提供了部署文档和使用指南，旨在帮助开发者轻松在Zephyr平台上扩展Phytium系列CPU的功能。本项目适合所有希望在Zephyr实时操作系统上充分利用Phytium系列CPU能力的开发者。

## 1.2 Zephyr 简介

[Zephyr Project Documentation — Zephyr Project Documentation](https://docs.zephyrproject.org/latest/index.html)

Zephyr是一个开源的实时操作系统（RTOS），主要面向在资源受限的嵌入式设备上运行。它由Linux Foundation托管，目的是提供一个轻量级、模块化的系统，以支持物联网（IoT）和边缘计算应用。Zephyr项目自2016年开始，迅速发展成为一个活跃的开源社区，汇集了来自全球的开发者和公司贡献代码和知识。下面是根据您提到的几个方面对Zephyr系统的介绍：

### 内核特点

1. **轻量级和高度模块化**：Zephyr设计为可配置且可扩展，允许开发者根据需要选择包含在最终固件中的组件，这有助于减少资源消耗。
2. **实时性**：作为RTOS，Zephyr提供实时任务调度，确保关键任务能够及时准确地执行。
3. **多平台支持**：支持多种不同架构的CPU芯片，包括ARM、Intel x86、RISC-V等，涵盖从低功耗微控制器到更复杂的系统级芯片（SoC）。
4. **安全性**：提供了包括数据加密和安全启动在内的多种安全特性，以保护设备免受攻击。

### 应用开发

1. **灵活的开发环境**：Zephyr可以通过其SDK和集成的开发环境（IDE）插件进行开发，支持多种编程语言，包括C和C++。
2. **设备驱动和中间件支持**：提供丰富的设备驱动程序和中间件，支持网络、蓝牙、文件系统等多种通信协议和服务。
3. **微内核和单片机（MCU）内核选项**：开发者可以根据应用需求，选择适合的内核配置，以最佳方式利用硬件资源。

### 生态特色

1. **强大的社区支持**：作为一个活跃的开源项目，Zephyr拥有广泛的社区支持，提供了大量的文档、教程和案例，帮助开发者快速上手。
2. **与主流技术兼容**：Zephyr可以与多种主流的开源技术栈整合，如MCUboot引导加载器、Mbed TLS安全库等，确保开发者能够构建安全、可靠的应用。
3. **跨行业应用**：适用于多种应用场景，包括但不限于智能家居、可穿戴设备、工业控制和汽车电子。

## 1.3 适配框架

本开源项目由3个仓库（图1）组成，分别是Phytium-Zephyr-SDK、Zephyr OS、Standalone-For-Zephyr。以下是关于这三个开源仓库的介绍。

Phytium-Zephyr-SDK：此仓库作为整个开源项目的入口，提供了项目说明、部署手册、版本管理、测试例程等开发者在调试过程中会使用到的必要内容。开发者如果在部署开发过程中如果了问题，可以在此仓库下的issue下提交给我们。

Zephyr OS: 此仓库作为Zephyr 官方的镜像站使用，其中会包含Phytium 系列CPU 的设备树以及相应的驱动代码和配置文件。

Standalone-For-Zephyr：此仓库作为Zephyr项目依赖模块（modules）中的硬件抽象层，是由Phytium提供的Phytium-Standalone-SDK与驱动适配层构成，此仓库为Zephyr整个构建过程提供驱动部分的软件底座。

![1712020444273](figs/readme/1712020444273.png)

    图1 项目整体架构图

## 2. 仓库使用说明

- 本例程目前仅应用于Ubuntu开发环境
- 本模块主要分为3大内容：1. 开发环境搭建；2. 软件部署；3. 例程使用；

### 2.1 开发环境搭建

1. 升级ubuntu环境，建议使用Ubuntu22.04版本

```
sudo apt update
sudo apt upgrade
```

2. 如果使用的是早于22.04版本的Ubuntu，为了满足上述主要依赖项的最低版本要求，需要添加额外的软件仓库。在这种情况下，下载、检查并执行Kitware存档脚本，以将Kitware APT仓库添加到你的软件源列表中。关于kitware-archive.sh的详细解释可以在这里找到：kitware第三方apt仓库。

```
sudo bash ./tools/kitware-archive.sh
```

3. 通过apt 安装必备软件

```
sudo apt install --no-install-recommends git cmake ninja-build gperf \
  ccache dfu-util device-tree-compiler wget \
  python3-dev python3-pip python3-setuptools python3-tk python3-wheel xz-utils file \
  make gcc gcc-multilib g++-multilib libsdl2-dev libmagic1
```

4. 检查必备的软件否成功

- 请确保当前安装版本符合要求，备注为最低版本要求
```
cmake --version    (minimum required is "3.20")
python3 --version  (minimum required is "3.8")
dtc --version      (minimum required is "1.4.6")
```

5. 安装python3虚拟化环境

```
sudo apt install python3-venv
```

6. 安装west 软件

```
pip install west
```

7. 安装Zephyr SDK软件

- 可在[Zephyr SDK bundle](https://github.com/zephyrproject-rtos/sdk-ng/releases/tag/v0.16.5-1)中手动下载并放置在 ~/ 文件夹下，然后从第三步开始执行

![zephy_sdk_install](figs/readme/zephy_sdk_install.png)

```
cd ~
wget https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v0.16.5-1/zephyr-sdk-0.16.5-1_linux-x86_64.tar.xz
wget -O - https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v0.16.5-1/sha256.sum | shasum --check --ignore-missing
tar xvf zephyr-sdk-0.16.5-1_linux-x86_64.tar.xz
cd zephyr-sdk-0.16.5-1
./setup.sh
```

### 2.2 软件部署

1. 拉取本仓库代码到一个合适的目录，一般来说，代码目录名称为：phytium-zephyr-sdk

2. 进入sdk目录，创建一个Python虚拟化环境

```
cd phytium-zephyr-sdk
python3 -m venv ./.venv
```

3. 通过source 激活您的虚拟环境

```
source ./.venv/bin/activate
```

4. 在当前目录下执行以下命令初始化Zephyr OS，后续Zephyr OS 将会保存在zephyr目录下

```
./tools/pull_zephyr_os.sh 
```

5. 更新Zephyr OS中的其他项目，这个过程可能会耗时较长，请耐心等待
>如果出现某些项目更新中断或失败，可以尝试再次使用命令进行更新

```
west update
```

6. 安装必备软件至python虚拟环境中，这个过程可能会耗时较长，请耐心等待
>如果出现某些软件安装中断或失败，可以尝试再次使用命令进行安装

```
pip install -r zephyr/scripts/requirements.txt
```

## 3. 如何部署一个软件

详细的实现方法请参考 [Application Development — Zephyr Project Documentation](https://docs.zephyrproject.org/latest/develop/application/index.html)

- 部署软件最好的方法是从 Phytium-Zephyr-SDK 例子或者Zephyr 官方的例子出发进行部署与构建 。

Zephyr官方的例程路径为：zephyr/sample
Phytium-Zephyr-SDK 提供的例程路径为 ： app/

## 3.1 应用程序概述

Zephyr 的构建系统基于 CMake。

该构建系统以应用程序为中心，要求基于 Zephyr 的应用程序启动构建 Zephyr 源代码。应用程序构建控制了应用程序本身和 Zephyr 的配置和构建过程，将它们编译成一个单独的二进制文件。

主要的 zephyr 仓库包含了 Zephyr 的源代码、配置文件和构建系统。您可能还安装了各种模块（外部项目）与 zephyr 仓库一起，这些模块提供了第三方源代码集成。

应用程序目录中的文件将 Zephyr 和任何模块与应用程序连接起来。该目录包含所有特定于应用程序的文件，如特定于应用程序的配置文件和源代码。

以下是一个简单的 Zephyr 应用程序中的文件：

```

<app>
├── CMakeLists.txt
├── app.overlay
├── prj.conf
├── VERSION
└── src
    └── main.c
```

这些内容包括：

CMakeLists.txt：这个文件告诉构建系统在哪里找到其他应用程序文件，并将应用程序目录与 Zephyr 的 CMake 构建系统链接起来。这种链接提供了 Zephyr 构建系统支持的功能，如特定于板子的配置文件，运行和调试在真实或模拟硬件上编译的二进制文件的能力等等。

app.overlay：这是一个设备树覆盖文件，指定应用于任何构建板的基础设备树的应用程序特定更改。设备树覆盖的目的通常是配置应用程序使用的硬件相关内容。

构建系统默认会查找 app.overlay，但您可以添加更多设备树覆盖，还会搜索其他默认文件。

prj.conf：这是一个 Kconfig 片段，用于指定一个或多个 Kconfig 选项的应用程序特定值。这些应用程序设置与其他设置合并，以产生最终配置。Kconfig 片段的目的通常是配置应用程序使用的软件功能。

构建系统默认会查找 prj.conf，但您可以添加更多 Kconfig 片段，还会搜索其他默认文件。

有关 Kconfig 配置的更多信息，请参阅下文的 Kconfig 配置部分。

VERSION：一个包含几个版本信息字段的文本文件。这些字段让您管理应用程序的生命周期，并在签名应用程序镜像时自动提供应用程序版本。

有关此文件及其使用方式的更多信息，请参阅应用程序版本管理。

main.c：一个源代码文件。应用程序通常包含用 C、C++ 或汇编语言编写的源文件。Zephyr 的惯例是将它们放在 `<app>` 的一个名为 src 的子目录中。

一旦定义了应用程序，您将使用 CMake 生成一个构建目录，其中包含构建应用程序和 Zephyr 所需的文件，然后将它们链接成一个最终二进制文件，您可以在板子上运行。这样做的最简单方法是使用 west build，但您也可以直接使用 CMake。应用程序构建工件始终生成在一个单独的构建目录中：Zephyr 不支持“树内”构建。

## 3.2 构建与重建应用程序

Zephyr的构建系统将应用程序的所有组件编译和链接成一个单独的应用程序镜像，可以在模拟硬件或实际硬件上运行。

与任何其他基于CMake的系统一样，构建过程分为两个阶段。首先，使用cmake命令行工具生成构建文件（也称为构建系统），同时指定一个生成器。该生成器确定构建系统将在第二阶段使用的本地构建工具。第二阶段运行本地构建工具，实际构建源文件并生成一个镜像。要了解更多关于这些概念的信息，请参考官方CMake文档中的CMake简介。

尽管Zephyr的默认构建工具是west，Zephyr的元工具，它在后台调用cmake和底层构建工具（ninja或make），但您也可以选择直接调用cmake，如果您更喜欢的话。在Linux和macOS上，您可以在make和ninja生成器（即构建工具）之间进行选择，而在Windows上，您需要使用ninja，因为该平台不支持make。为简单起见，本指南将在整个过程中使用ninja，并且如果您选择使用west build构建应用程序，则需要知道它在幕后默认使用ninja。

- 在构建过程中，我们常用的构建方法为：

```
west build -b <board_name> <project_path> -DOVERLAY_CONFIG=<prj.conf>
```

上面west 命令中 ，board_name 代表开发板的型号，project_path 代表需要编译例程的相对路径 ，prj.conf为Kconfig 片段 。

一个具体示例为： 在app/system/kernel_feature/schedule 路径下，使用 “west build -b e2000q_demo ./ -DOVERLAY_CONFIG=prj.conf” 命令，将会使用e2000q_demo 开发板，编译app/system/kernel_feature/schedule 下的源码例程，并且采用当前路径下的prj.conf 配置。

- 我们常用的清除指令为：

```
west build -t clean
```

clean 指令 ，主要为了清除 `<app>/build`下的编译缓存信息，以这样的方式实现构建系统的重建 。

## 4. 硬件支持情况

| 硬件平台          | 支持的架构   | 备注             |
|-----------------|------------|----------------|
| E2000D/Q DEMO板 | aarch64    | 支持SMP模式     |
| E2000D/Q DEMO板 | aarch32    |                |

## 5. 例程展示

### 5.1 内核测试

[schedule](./app/system/kernel_feature/schedule/README.md)
[interrupt](./app/system/interrupt/README.md)
[smp_test](./app/system/smp/smp_test/README.md)

### 5.2 peripheral测试

[echo](./app/peripheral/net/echo/README.md)
[telnet](./app/peripheral/net/telnet/README.md)
[tf_test](./app/peripheral/sdio/tf_test/README.md)

## 例程介绍

# FIQ

Q：在phytium-zephyr-sdk目录中使用```west update```命令时，出现某个仓库下载超时的情况，应该如何处理？

A：zephyr依赖的大部分modules仓库部署在github上，请确保您的网络能够正常github

Q：如果出现以下打印应该如何解决：

  ![1712058336378](figs/readme/1712058336378.png)

A：在拉取Zephyr OS的时候，出现了错误，需要先将当前目录下的zephyr目录删除，然后再重复初始化Zephyr OS

Q：当执行```pip install -r /zephyr/scripts/requirements.txt```命令时出现以下超时问题时，该怎么办：

  ![1712107861173](figs/readme/1712107861173.png)

A：可以在```pip install -r /zephyr/scripts/requirements.txt```后面追加```-i https://pypi.tuna.tsinghua.edu.cn/simple```，将pip源设置为国内源