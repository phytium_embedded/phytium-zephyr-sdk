# Phytium-Zephyr-SDK 2024-06-26 v1.0.0 ChangeLog

Change Log since 2024-06-26

## README

- update version number

# Phytium-Zephyr-SDK 2024-06-21 ChangeLog

Change Log since 2024-06-17

## app

- add const key word to one variable

## README

- update README.md, remove the description about /zephyeproject, add some detail notes

# Phytium-Zephyr-SDK 2024-06-21 ChangeLog

Change Log since 2024-06-17

## app

- modified every CMakeLists.tx in peripheral apps to resolve the issue of inability to compile due to repository merging.

## modules

- standalone_for_zephyr repository merge into Phytium-zephyr-sdk repository

# Phytium-Zephyr-SDK 2024-06-13 ChangeLog

Change Log since 2024-06-07

## app

- add pcie enum example

# Phytium-Zephyr-SDK 2024-05-08 ChangeLog

Change Log since 2024-04-11

## doc

- modify the bugs in the documentation

# Phytium-Zephyr-SDK 2024-04-11 ChangeLog

Change Log since 2024-04-11

## doc 

- add readme

## example 

- add xmac echo example